TARGET = qtsensors_webos
QT = sensors core

HEADERS += qwebossensorcommon.h \
           qwebosaccelerometer.h

SOURCES += qwebossensorcommon.cpp \
           qwebosaccelerometer.cpp \
           qwebossensorplugin.cpp

OTHER_FILES = webos.json

unix:!darwin:!qnx:!android:!openbsd: LIBS += -lrt
LIBS += -lSDL -lpdl

PLUGIN_TYPE = sensors
PLUGIN_CLASS_NAME = QWebOSSensorPlugin
load(qt_plugin)
