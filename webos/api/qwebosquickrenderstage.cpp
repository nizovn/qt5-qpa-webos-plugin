/****************************************************************************
**
** Copyright (C) 2016 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the plugins of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 3 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL3 included in the
** packaging of this file. Please review the following information to
** ensure the GNU Lesser General Public License version 3 requirements
** will be met: https://www.gnu.org/licenses/lgpl-3.0.html.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 2.0 or (at your option) the GNU General
** Public license version 3 or any later version approved by the KDE Free
** Qt Foundation. The licenses are as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL2 and LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-2.0.html and
** https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "qwebosquickrenderstage_p.h"

QT_BEGIN_NAMESPACE

bool QWebOSQuickRenderStage::render()
{
    // based on https://github.com/qt/qtdeclarative/blob/5.9/src/quick/items/qquickwindow.cpp#L451

    if ((!qp) || (!qweb)) return true;

    QQuickWindow *q = qp->q_func();
    if (!q) return true;

    QWebOSScreen *screen = qweb->screen();
    if (!screen) return true;

    int fboId = 0;
    const qreal devicePixelRatio = q->effectiveDevicePixelRatio();
    QSize size = q->geometry().size();

    int rotation = screen->angleBetween(screen->nativeOrientation(), screen->orientation());
    QMatrix4x4 rotationMatrix;
    rotationMatrix.setToIdentity();
    rotationMatrix.rotate(rotation, 0, 0, 1);

    if (qp->renderTargetId) {
        QRect rect(QPoint(0, 0), qp->renderTargetSize);
        fboId = qp->renderTargetId;
        qp->renderer->setDeviceRect(rect);
        qp->renderer->setViewportRect(rect);
        if (QQuickRenderControl::renderWindowFor(q)) {
            qp->renderer->setProjectionMatrixToRect(QRect(QPoint(0, 0), size));
            qp->renderer->setDevicePixelRatio(devicePixelRatio);
        } else {
            qp->renderer->setProjectionMatrixToRect(QRect(QPoint(0, 0), rect.size()));
            qp->renderer->setDevicePixelRatio(1);
        }
    }
    else {
        QRect rect = screen->geometry();

        if ((rotation == 90) || (rotation == 270))
            rect = rect.transposed();

        qp->renderer->setDeviceRect(rect);
        qp->renderer->setViewportRect(rect);
        qp->renderer->setProjectionMatrixToRect(QRect(QPoint(0, 0), size));
        qp->renderer->setDevicePixelRatio(devicePixelRatio);

        QMatrix4x4 target = qp->renderer->projectionMatrix();
        target = rotationMatrix * target;
        qp->renderer->setProjectionMatrix(target);
    }

    qp->context->renderNextFrame(qp->renderer, fboId);

    return true;
}

bool QWebOSQuickRenderStage::swap()
{
    return false;
}

QT_END_NAMESPACE
