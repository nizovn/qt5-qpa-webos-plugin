/****************************************************************************
**
** $QT_BEGIN_LICENSE:LGPL$
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Palm gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef QWEBOSCLIPBOARD_H
#define QWEBOSCLIPBOARD_H

#include "qwebosglobal_p.h"

#include <qpa/qplatformclipboard.h>
#include <QMimeData>

QT_BEGIN_NAMESPACE

class Q_WEBOS_EXPORT QWebOSClipboard : public QPlatformClipboard {
    public:
        QMimeData *mimeData(QClipboard::Mode mode = QClipboard::Clipboard) override;
        void setMimeData(QMimeData *data, QClipboard::Mode mode = QClipboard::Clipboard) override;
        bool supportsMode(QClipboard::Mode mode) const override;

    private:
        QMimeData m_data;
        // Note that the clipboard path seems to have been
        // /tmp/webkit-clipboard in older OS versions, presumably in webOS 1.
        static constexpr const char *clipboardPath = "/var/tmp/webkit-clipboard";
};

QT_END_NAMESPACE

#endif
