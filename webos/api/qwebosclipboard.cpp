/****************************************************************************
**
** $QT_BEGIN_LICENSE:LGPL$
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Palm gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "qwebosclipboard_p.h"

#include <QMimeData>
#include <QFile>

QMimeData *QWebOSClipboard::mimeData(QClipboard::Mode mode) {
    Q_UNUSED(mode);
    QFile file(clipboardPath);
    file.open(QIODevice::ReadOnly);
    m_data.setText(file.readAll());
    return &m_data;
}

void QWebOSClipboard::setMimeData(QMimeData *data, QClipboard::Mode mode) {
    Q_UNUSED(mode);
    QFile file(clipboardPath);
    file.open(QIODevice::WriteOnly);
    file.write(data->text().toUtf8());
}

bool QWebOSClipboard::supportsMode(QClipboard::Mode mode) const {
    return mode == QClipboard::Clipboard;
}
